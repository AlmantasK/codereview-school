﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace RepositoryLib.Base
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> FindAll();
        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
