﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Microsoft.EntityFrameworkCore;
using RepositoryLib.Base;

namespace RepositoryLib.Data
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User GetUserByEmail(string email);
    }

    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        protected DbSet<User> RepositoryContext { get; set; }

        public UserRepository(SchoolDbContext repositoryContext) : base(repositoryContext)
        {
            RepositoryContext = repositoryContext.Set<User>();
        }

        public User GetUserByEmail(string email)
        {
            return RepositoryContext.SingleOrDefault(x => x.Email.ToLower().Equals(email.Trim().ToLower()));
        }
    }
}
