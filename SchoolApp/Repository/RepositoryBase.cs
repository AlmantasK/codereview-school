﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using RepositoryLib.Data;
using SchoolApp.Data;

namespace SchoolApp.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected SchoolDbContext RepositoryContext { get; set; }

        public RepositoryBase(SchoolDbContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public IEnumerable<T> FindAll()
        {
            return this.RepositoryContext.Set<T>();
        }

        public IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.RepositoryContext.Set<T>().Where(expression);
        }

        public void Add(T entity)
        {
            this.RepositoryContext.Set<T>().Add(entity);
            Save();
        }

        public void Update(T entity)
        {
            this.RepositoryContext.Set<T>().Update(entity);
            Save();
        }

        public void Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
            Save();
        }

        public void Save()
        {
            this.RepositoryContext.SaveChanges();
        }
    }
}
