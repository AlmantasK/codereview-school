﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SchoolApp.System
{
    public class AuthHash
    {
        public static string MakeHash(string value)
        {
            return Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value)));
        }

        public static bool CompareHash(string plainString, string hashString)
        {
            if (MakeHash(plainString) == hashString)
            {
                return true;
            }

            return false;
        }
    }
}
